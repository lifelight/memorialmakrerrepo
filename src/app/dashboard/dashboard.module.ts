import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { DropdownModule } from 'ng2-bootstrap/dropdown';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { FormWizardModule } from 'angular2-wizard';
import { CommonModule } from '@angular/common';
import { ImageUploadModule } from "angular2-image-upload";
import * as $ from 'jquery';


@NgModule({
  imports: [
    FormsModule,      
    DashboardRoutingModule,
    ChartsModule,
    DropdownModule,
    FormWizardModule,
    CommonModule,
    ImageUploadModule.forRoot(),
  
  ],
  declarations: [ DashboardComponent ]
})
export class DashboardModule { }
