import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';


@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['./style12.css']
})

export class DashboardComponent implements OnInit {
   

 step:string;

 email: string;
 
  // constructor( ) { }
  ngOnInit(): void {
    // generate random values for mainChart

    $(document).ready(function () {




        
      var navListItems = $('div.setup-panel div a'),
          allWells = $('.setup-content'),
          allNextBtn = $('.nextBtn');
      var prev = $('.prevBtn');
  
      allWells.hide();
  
      navListItems.click(function (e) {
          e.preventDefault();
          var $target = $($(this).attr('href')),
              $item = $(this);
  
          if (!$item.hasClass('disabled')) {
              navListItems.removeClass('btn-success').addClass('btn-default');
              $item.addClass('btn-success');
              allWells.hide();
              $target.show();
              $target.find('input:eq(0)').focus();
          }
      });
  
      allNextBtn.click(function () {
          var curStep = $(this).closest(".setup-content"),
              curStepBtn = curStep.attr("id"),
              nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
              curInputs = curStep.find("input[type='text'],input[type='url']"),
              isValid = true;
  
       
  
          if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
      });

      prev.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

     

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
    });

  
      $('div.setup-panel div a.btn-success').trigger('click');
  
      
      
      
    //   ******************
    //   $("#image_2").hide();
    var selected_image_ids = new Array;

	// keep track of what images we checked off
	$(".image_checkbox").click(function () {

		if ($(this).attr("checked") == "checked")
		{		
			selected_image_ids.push($(this).attr("id"));
		}
		else {
			// remove the items from the array - far too complex for what it needs to do.
			for(var i = selected_image_ids.length-1; i >= 0; i--)
			{
				if(selected_image_ids[i] == $(this).attr("id")){
					selected_image_ids.splice(i,1);
				}
			}
		}

		//$("#selected_num").html(selected_image_ids.length);
	})


	$(".drop_div ul li").droppable({
		accept: ".image_list li",
        hoverClass: "active",
        // revert:true;
		drop: function( event, ui ) {


			$(ui.helper).remove(); //destroy clone
            $(ui.draggable).remove(); //remove from list

			var dropped_object = $(this);
			var image_collection = dropped_object.contextmenu;

			$("#status").html("Success! You've moved: " + selected_image_ids.length + " images to: " + image_collection);

			for(var i = selected_image_ids.length-1; i >= 0; i--)
			{
				$("#image_" + selected_image_ids[i]).parent().parent().fadeOut();
			}

			selected_image_ids = new Array;

		}
	});


	$(".image_list").sortable({

		//refreshPositions: true,
		scroll: true,
		delay: 100,
		cursor: "move",
		revert: true,
		helper: "clone",

		start: function (event, ui)
		{
			if (selected_image_ids.length > 1) {

				var image_string = "";
				var max_images = 2;
				var class_counter = 0;

				if (selected_image_ids.length == 2) {
					max_images = 1;
				}

				for (var i=0; i<=max_images; i++)
				{
					var image_link = $("#image_" + selected_image_ids[i]).attr("src");
					class_counter = i+1;
					image_string += '<img src="' + image_link + '" class="photo_helper" id="photo_helper' + class_counter + '" />'
				}

				$(ui.helper).html('<div id="photoHelper">' + image_string + '</div>');
			}

		}
	}); 




  
    });
  }
        // ***************** jquery for image random

}   
      